package com.gfg.session.practice2.resource;


import com.gfg.session.practice2.Exceptions.InvalidStudentInfoException;
import com.gfg.session.practice2.controller.StudentManager;
import com.gfg.session.practice2.model.StudentInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("api")
public class ResourcePage {

  @Autowired
  StudentManager studentManager;

  @GetMapping("/hello/comp")
  public String getData0(){
    return studentManager.getInfo();
  }

  @GetMapping("/hello1/{name}")
  public ResponseEntity<Object> getData(@PathVariable("name") String name){
    return new ResponseEntity<>("Hello "+name+" welcome to GFG...", HttpStatus.ACCEPTED);
  }


  @GetMapping("/hello")
  public ResponseEntity<Object> getData2(@RequestParam String name, @RequestParam int age){
    return new ResponseEntity<>("Hello "+name+" welcome to GFG...\nyour age is "+age, HttpStatus.ACCEPTED);
  }

  @GetMapping("/hello/2/{name}")
  public ResponseEntity<Object> getData3(@PathVariable String name, @RequestParam int age){
    if(name.equals(" ") || age<18){
      throw new InvalidStudentInfoException();
    }
    return new ResponseEntity<>("Hello "+name+" welcome to GFG...\nyour age is "+age,HttpStatus.OK );
  }

  @GetMapping("/hello2")
  public ResponseEntity<Object> getData4(@RequestBody StudentInfo studentInfo){

//    return "Hello "+studentInfo.getName()+" of branch "+studentInfo.getBranch()+" welcome to GFG...\nyour age is "+studentInfo.getAge();
    return new ResponseEntity<>("Hello "+studentInfo.getName()+" of branch "+studentInfo.getBranch()+" welcome to GFG...\nyour age is "+studentInfo.getAge(),HttpStatus.OK);
  }

  @GetMapping("/endpoint")
  public String getHeaderValue(HttpServletRequest request) {
    String name = request.getHeader("name");
    String age = request.getHeader("age");
    // Process the header value
    return "Header Value: " + name + " " + age;
  }




}
