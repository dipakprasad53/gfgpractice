package com.gfg.session.practice2.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class StudentInfo {
  private String name;

  private String branch;
  private int age;
}
