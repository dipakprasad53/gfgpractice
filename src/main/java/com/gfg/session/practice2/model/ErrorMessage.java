package com.gfg.session.practice2.model;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class ErrorMessage {
  private String message;
  private String status;
  private long dateTime;

}
