//package com.gfg.session.mysql.resource;
//
//import com.gfg.session.mysql.datastore.ItemDataStore;
//import com.gfg.session.mysql.model.Student;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//@RestController
//@RequestMapping("database/api")
//public class StudentResource {
//
//  @Autowired
//  private ItemDataStore studentStore;
//
//  @PostMapping("/insert")
//  public ResponseEntity<Object> insertStudent(@RequestBody Student student){
//    return new ResponseEntity<>(studentStore.addnewStudent(student), HttpStatus.ACCEPTED);
//  }
//
//  /*
//  git commit --all -m ""
//
//  Homework
//  Try to implement api's to
//  1: get list of all students.
//  2: get studentinfo using studentId.
//  3: api to delete student.
//  4: api to update student info.
//   */
//}
