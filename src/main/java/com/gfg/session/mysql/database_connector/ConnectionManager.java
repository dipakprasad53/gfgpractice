package com.gfg.session.mysql.database_connector;

import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

@Component
public class ConnectionManager {

  public static Connection getConnection() {
    try {
      Connection con = DriverManager.getConnection(
          "jdbc:mysql://localhost:3306/backendgfg", "root", "");

//      Statement stmt=con.createStatement();

//      stmt.executeUpdate("INSERT INTO student (student_name, age ) VALUES('Adnan Afridi', 25)");
//      int result1=stmt.executeUpdate("UPDATE student SET address = 'Delhi' WHERE studentId = 4;");
//      int result2=stmt.executeUpdate("delete from student where studentId=3");
//
//      System.out.println(result1+" records affected");

      return con;
    } catch (Exception e) {
      System.out.println(e);
    }
    return null;
  }

}
