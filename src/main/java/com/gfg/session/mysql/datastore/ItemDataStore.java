package com.gfg.session.mysql.datastore;

import com.gfg.session.mysql.database_connector.ConnectionManager;
import com.gfg.session.mysql.model.Item;
import com.gfg.session.mysql.model.Student;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@Service
public class ItemDataStore {
  private static final String INSERT_STUDENT = "insert into student(student_name,address,age) values(?,?,?)";

  private static final String GET_STUDENT_DATA = "select * from student where student_name = ?";
  private static final String GET_ITEM_COUNT = "select availableCopies from item where name=? and author=? and publisher=? and category=?";

  private static final String UPDATE_ITEM = "update table item set availableCopies = ? where name=? and author=? and publisher=? and category=?";
  private Connection con = ConnectionManager.getConnection();


  public Student addnewStudent(Student student) {

    String message = "";
    try {
      PreparedStatement ps = con.prepareStatement(INSERT_STUDENT);
      ps.setString(1, student.getName());
      ps.setString(2, student.getBranch());
      ps.setInt(3, student.getStudentAge());
      int result = ps.executeUpdate();
      if (result == 1) {
        message = "Student inserted successfully...";
        PreparedStatement ps2 = con.prepareStatement(GET_STUDENT_DATA);
        ps2.setString(1, student.getName());
        ResultSet rs = ps2.executeQuery();
        while(rs.next()) {
          return Student.builder()
              .studentId(rs.getInt("studentId"))
              .name(rs.getString("student_name"))
              .branch(rs.getString("branch"))
              .studentAge(rs.getInt("age"))
              .build();
        }
      }
    } catch (Exception e) {
      System.out.println("Error while inserting new student : " + e);
    }

    return Student.builder().build();
  }

//  public static void main(String[] args) {
//
//    ItemDataStore.addnewStudent(Student.builder().name("Abhay").branch("Mechanical").studentAge(26).build());
//  }

//    int updateCount = 0;
//    try {
//      boolean isAvailable = false;
//      int copies = checkIfItemAlreadyExist(item,isAvailable);
//      if(isAvailable){
//        PreparedStatement ps = con.prepareStatement(UPDATE_ITEM);
//        ps.setInt(1, copies+1);
//        ps.setString(2, item.getName());
//        ps.setString(3,item.getAuthor());
//        ps.setString(4,item.getPublisher());
//        ps.setString(5,item.getCategory());
//        updateCount = ps.executeUpdate();
//        if(updateCount>0){
//          System.out.println("item updated successfully");
//        }else{
//          System.out.println("failed to update");
//        }
//      }else{
//        PreparedStatement ps = con.prepareStatement(INSERT_ITEM);
//        // set the preparedstatement parameters
//        ps.setString(1, item.getName());
//        ps.setString(2,item.getAuthor());
//        ps.setString(3,item.getPublisher());
//        ps.setString(4,item.getCategory());
//        // call executeUpdate to execute our sql update statement and returns number of rows affected
//        updateCount = ps.executeUpdate();
//        //step4 execute query
//        if(updateCount>0){
//          System.out.println("item inserted successfully");
//        }else{
//          System.out.println("failed to insert");
//        }
//      }
//
//      //step5 close the connection object
//    }catch (SQLException sqlException){
//      System.out.println(sqlException.getMessage());
//    }
//    return updateCount;
//  }

  public int checkIfItemAlreadyExist(Item item, boolean isAvailable){
    int currentCopies = 0;
    try {
      PreparedStatement ps = con.prepareStatement(INSERT_STUDENT);
      // set the preparedstatement parameters
      ps.setString(1,item.getName());
      ps.setString(2,item.getAuthor());
      ps.setString(3,item.getPublisher());
      // call executeUpdate to execute our sql update statement and returns number of rows affected
      ResultSet rs = ps.executeQuery(GET_ITEM_COUNT);
      //step4 execute query

      while(rs.next()){
        currentCopies = rs.getInt(1);
        isAvailable = true;
      }
      //step5 close the connection object
    }catch (SQLException sqlException){
      System.out.println(sqlException.getMessage());
    }
    return currentCopies;
  }

}
