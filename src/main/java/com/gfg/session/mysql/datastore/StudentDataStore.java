package com.gfg.session.mysql.datastore;

import static org.apache.logging.log4j.LogManager.getLogger;

import com.gfg.session.mysql.database_connector.ConnectionManager;
import com.gfg.session.mysql.model.Student;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class StudentDataStore {

  private static final Logger log = getLogger();
  private static final String INSERT_STUDENT = "insert into student(name,contact,branch,password) values(?,?,?,?)";

  private static Connection con = ConnectionManager.getConnection();

  public static void addnewStudent(Student student){
    try {
      PreparedStatement ps = con.prepareStatement(INSERT_STUDENT);
      // set the preparedstatement parameters
      ps.setString(1,student.getName());
      ps.setString(2,student.getContact());
      ps.setString(3,student.getBranch());
      ps.setString(4,student.getPassword());
      // call executeUpdate to execute our sql update statement and returns number of rows affected
      int updateCount = ps.executeUpdate();
      //step4 execute query
      if(updateCount>0){
        System.out.println("student inserted successfully");
      }else{
        System.out.println("failed to insert");
      }
      //step5 close the connection object
    }catch (SQLException sqlException){
      System.out.println(sqlException.getMessage());
    }
  }

}
