package com.gfg.session.mysql.model;

import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@Builder
public class Student {

  private int studentId;

  private String name;

  private String contact="9876554321";

  private String branch;

  private String accountStatus = "ACTIVE";

  private String registeredOn;

  private String password;

  private int studentAge;

}
