package com.gfg.session.mysql.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Item {

  private int itemId;

  private String name;

  private String author;

  private String publisher;

  private String category;

  private int availableCopies;

}
