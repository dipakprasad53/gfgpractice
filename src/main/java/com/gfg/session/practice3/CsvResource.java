package com.gfg.session.practice3;

import com.gfg.session.library_management_system.entities.Book;
import com.gfg.session.library_management_system.exceptions.InvalidRequestException;
import com.gfg.session.library_management_system.services.BookService;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;

@RequestMapping("csv")
@RestController
public class CsvResource {

  @Autowired
  private BookService bookService;

  @PostMapping("/parse_csv")
  public ResponseEntity<Object> parseCSV(HttpServletRequest httpServletRequest)
      throws ServletException, IOException {
    Part textPart = httpServletRequest.getPart("enterprise_name");
    Part filePart = httpServletRequest.getPart("gfg_csv");
    InputStream inputStream = textPart.getInputStream();
    String result = IOUtils.toString(inputStream, StandardCharsets.UTF_8);
    InputStream fileInputStream = filePart.getInputStream();
    CSVFormat csvFormat = CSVFormat.DEFAULT;
    CSVParser csvParser = new CSVParser(new InputStreamReader(fileInputStream),csvFormat);
    List<CSVRecord> rows = csvParser.getRecords().stream().skip(1).collect(Collectors.toList());
    List<Book> rec = new ArrayList<>();
    for (CSVRecord csv: rows) {
      String name = csv.get(0);
      String author = csv.get(1);
      if(name!=null && author!=null) {
        rec.add(Book.builder()
            .name(name)
            .author(author)
            .allocatedStatus(false)
            .build()
        );
      }else{
        throw new InvalidRequestException();
      }
    }
    Iterable<Book> bookList =  bookService.addBookList(rec);
    System.out.println(filePart.getName());
    System.out.println(filePart.getSubmittedFileName());
    System.out.println(textPart.getName());
    return new ResponseEntity<>(bookList, HttpStatus.OK);

  }
}
