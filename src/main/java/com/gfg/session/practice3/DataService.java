package com.gfg.session.practice3;

public interface DataService {
  int[] retrieveData();
}
