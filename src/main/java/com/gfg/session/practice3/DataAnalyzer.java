package com.gfg.session.practice3;


public class DataAnalyzer {
  private final DataService dataService;

  public DataAnalyzer(DataService dataService) {
    this.dataService = dataService;
  }

  public int findMax() {
    int[] data = dataService.retrieveData();
    int max = Integer.MIN_VALUE;
    for (int num : data) {
      if (num > max) {
        max = num;
      }
    }
    return max;
  }
}