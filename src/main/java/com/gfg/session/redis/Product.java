package com.gfg.session.redis;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.redis.core.RedisHash;

import java.io.Serializable;

@RedisHash("Product")
@ToString
@Getter
@Setter
public class Product implements Serializable {
  private int id;
  private String name;
  private int qty;
  private long price;
}
