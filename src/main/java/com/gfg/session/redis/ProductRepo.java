package com.gfg.session.redis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ProductRepo {
  public static final String HASH_KEY = "Product";

  @Autowired
  private RedisTemplate<String, Object> template;

  public Product save(Product product){
    template.opsForHash().put(HASH_KEY,product.getId(),product);
    return product;
  }

  public List<Object> getAll(){
    return (template.opsForHash().values(HASH_KEY));
  }


}
