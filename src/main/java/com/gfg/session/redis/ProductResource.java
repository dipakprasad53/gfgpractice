package com.gfg.session.redis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("redis/product")
public class ProductResource {
  @Autowired
  private ProductRepo productRepo;

  @PostMapping("/save")
  public Product save(@RequestBody Product product){
    System.out.println(product.toString());
    return productRepo.save(product);
  }

  @GetMapping("/getAll")
  public List<Object> getAll(){
    return productRepo.getAll();
  }

}
