package com.gfg.session.practice1;

public class Child extends Father{

  String play = "Badminton";
  public Child(){
    profession = "Engineer";
  }

  //overloading
  public int calculate(int a, int b, int c, String str){
    System.out.println(str);
    return a+b+c;
  }

  @Override
  public void isPainter(){
    System.out.println("Very bad at painting");
  }

  public void display(){
    System.out.print("I'm ");
    isPainter();
    System.out.print("My father ");
    super.isPainter();
  }
}
