package com.gfg.session.practice1;

public class SingletonExp {

  private static SingletonExp singletonExp;
  private static int check = 0;
  public int data;
  private SingletonExp(){
    check += 1;
    data = check;
    System.out.println("Instance "+check);
  }

  public static SingletonExp getMyInstance(){
    if(singletonExp == null){
      singletonExp = new SingletonExp();
    }
    return singletonExp;
  }
}

class Test3{
  public static void main(String[] args) {
//    SingletonExp s = new SingletonExp();
//    SingletonExp s2 = new SingletonExp();
    // Thread 1 ->
    SingletonExp s = SingletonExp.getMyInstance(); //----> Instance 1
    SingletonExp s2 = SingletonExp.getMyInstance(); //----> Instance 1
    SingletonExp s3 = SingletonExp.getMyInstance(); //----> Instance1
    // Thread 2 ->
    SingletonExp s4 = SingletonExp.getMyInstance(); //----> Instance 2
    SingletonExp s5 = SingletonExp.getMyInstance(); //----> Instance 2
    SingletonExp s6 = SingletonExp.getMyInstance(); //----> Instance 2

    System.out.println(s.data);
    System.out.println(s2.data);
    System.out.println(s3.data);

  }
}
