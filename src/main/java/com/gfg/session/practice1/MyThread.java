package com.gfg.session.practice1;

public class MyThread extends Thread {
  String name = "default";

  public MyThread(){

  }
  public MyThread(String name){
    this.name = name;
  }
  public void run(){
    try {
      Thread.sleep(1000);
    } catch (InterruptedException e) {
      throw new RuntimeException(e);
    }
    System.out.println(name);
  }
}
