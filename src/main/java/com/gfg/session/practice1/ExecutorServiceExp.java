package com.gfg.session.practice1;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ExecutorServiceExp {
  public static void main(String[] args) throws InterruptedException {

    ExecutorService executorService = Executors.newFixedThreadPool(3);
    executorService.submit(new MyThread("t1"));
    executorService.submit(new MyThread("t2"));
    executorService.submit(new MyThread("t3"));
    executorService.submit(new MyThread("t4"));
    executorService.submit(new MyThread("t5"));
    executorService.submit(new MyThread("t6"));
    executorService.submit(new MyThread("t7"));
    executorService.submit(new MyThread("t8"));
    executorService.submit(new MyThread("t9"));
    executorService.submit(new MyThread("t10"));
    Thread.sleep(5000);
//    executorService.shutdown();
    executorService.shutdownNow();

  }
}
