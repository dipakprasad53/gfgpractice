package com.gfg.session.practice1;

import java.util.Objects;

public class Emp {
  String empName;
  int empId;

  public Emp(String name, int id){
    empId=id;
    empName=name;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Emp emp = (Emp) o;
    return empId == emp.empId;
  }

  @Override
  public int hashCode() {
    return Objects.hash(empId);
  }

  @Override
  public String toString() {
    return empName+" plays ";
  }
}
