package com.gfg.session.practice1;

public class CustomeExceptionExp extends Exception{

  public CustomeExceptionExp(String msg){
    super(msg);
  }
}

class TestExcp{
  int age;
  public static void main(String[] args) {
    TestExcp testExcp = new TestExcp();
    try {
      testExcp.validate(12);
    }catch (Exception e){
      System.out.println(e);
      throw new RuntimeException("Invalid age request");
    }
  }
  public void validate(int age) /* throws CustomeExceptionExp */{
    try{
      if(age < 18) {
        throw new CustomeExceptionExp("Invalid Age");
      }
    }catch (CustomeExceptionExp e){
      System.out.println("Handled exception in validate method"+e);
    }
    this.age = age;
  }
}