package com.gfg.session.practice1;

import java.util.ArrayList;
import java.util.List;

abstract class AbstractionEcp {

  abstract public void diplay();

  public void diplay(String s){
    System.out.println(s);
  }
}

abstract class AbstractionEcp2 {

  public AbstractionEcp2(){

  }

  abstract public void diplay();

  public void diplay(String s){
    System.out.println(s);
  }
}
// static - default
interface Test{

  public void disp();
  default public void disp(String s){
    System.out.println(s);
  }

}

interface Test2{

  public static void test(){
    System.out.println();
  }
  public void disp();
  default public void disp(String s){
    System.out.println(s);
  }

}



class Imp extends AbstractionEcp implements Test,Test2{

  @Override
  public void diplay() {
    System.out.println("Implemented here");
  }

  @Override
  public void disp() {

  }

  @Override
  public void disp(String s) {
    Test.super.disp(s);
  }
}

class Imp2 extends AbstractionEcp{

  @Override
  public void diplay() {
    System.out.println("Implemented here again");
  }
}

class Imp3 extends AbstractionEcp{

  @Override
  public void diplay() {
    System.out.println("Implemented here again and again");
  }
}



class Main{
  public static void main(String[] args) {
    Imp imp1 = new Imp();
    AbstractionEcp imp = new Imp();
    imp.diplay();

    AbstractionEcp a = new Imp();
    AbstractionEcp b = new Imp3();
    AbstractionEcp c = new Imp2();

    List<AbstractionEcp> list = new ArrayList();
    list.add(a);
    list.add(b);
    list.add(c);


  }
}
