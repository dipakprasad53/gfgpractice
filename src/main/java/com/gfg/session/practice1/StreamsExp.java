package com.gfg.session.practice1;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class StreamsExp {

  public static void main(String[] args) {
    List<Integer> l = Arrays.asList(1,2,3,4,5,6,7,8,9,10);
    List<Integer> evenList = l.stream()
        .filter(d -> d%2 == 0)
        .collect(Collectors.toList());
    System.out.println(evenList);

    Optional<Integer> data = l.parallelStream().filter(dipak -> dipak%5 ==0).findFirst();
    if(data.isPresent()){
      System.out.println("data found : "+data.get());
    }else{
      System.out.println("Data not found");
    }

    StudentDB sd1 = new StudentDB("Umesh",1);
    StudentDB sd2 = new StudentDB("Maaz",2);
    StudentDB sd3 = new StudentDB("Aditya",3);
    StudentDB sd4 = new StudentDB("Mohit",4);

    List<StudentDB> ls = Arrays.asList(sd1,sd2,sd3,sd4);

    Optional<StudentDB> sd = ls.stream().filter(student -> student.roll == 5).findFirst();

    if(sd.isPresent()){
      System.out.println("Student found at roll 3 : "+sd.get().name);
    }else{
      System.out.println("Student not found");
    }



  }
}

class StudentDB{
  String name;
  int roll;
  public StudentDB(String n,int r){
    name = n;
    roll = r;
  }
}