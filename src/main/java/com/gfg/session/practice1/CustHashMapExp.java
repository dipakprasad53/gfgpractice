package com.gfg.session.practice1;

import java.util.HashMap;
import java.util.Map;

public class CustHashMapExp {



  public static void main(String[] args) throws InterruptedException {
    Map<String, String> map1 = new HashMap<>();

    System.out.println("Entered main function");
    Map<Emp, String> plays = new HashMap<>();

    Emp Emp1 = new Emp("Abhay Sharma",1000);
    Emp Emp2 = new Emp("Adnan Afridi",1001);
    System.out.println("Added 2 entries");
    Thread.sleep(1000);
    Emp Emp3 = new Emp("Anushka Sharma",1002);
    Emp Emp4 = new Emp("Anushka",1002);
    Emp Emp5 = new Emp("Anushka",1003);
    System.out.println("Added all entries");

    plays.put(Emp1,"Cricket");
    plays.put(Emp2,"Tennis");
    plays.put(Emp3, "Football");
    plays.put(Emp4, "Cricket");
    plays.put(Emp5, "Badminton");

    System.out.println(plays);
  }
}
