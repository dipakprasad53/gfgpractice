package com.gfg.session.practice1;

class Student {
  private String name;
  private int roll;
  public static int count;

  static {
    count = 0;
    System.out.println("Hello World");
  }

  public Student(){}
  public Student(String name){
    this.name = name;
//    Student.count = Student.count + 1;
    Student.count += 1;
    roll = Student.count;
    display();
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getRoll() {
    return roll;
  }

  public static void check(){
    System.out.println("Checked");
  }

  public void setRoll(int roll) {
    this.roll = roll;
  }

  public void display(){
    System.out.println("Name: "+name+" Roll: "+roll);
  }

  public static void main(String[] args) {
    check();
    Student student1 = new Student("Souvik");

    Student student2 = new Student("Kapil");
    Student student3 = new Student("Ramzan");
    Student student4 = new Student("Anushka");
  }
}
