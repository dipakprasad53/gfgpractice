package com.gfg.session.practice1;

public class PolymorphismExp {
  //Method overloading

  //return square of a number
  public int calculate(int a){
    return a*a;
  }

  //return subtraction
  public int calculate(int a, int b){
    return b-a;
  }

  //addition
  public int calculate(int a, int b, int c){
    return a+b+c;
  }

  //addition
  public int calculate(int a,String str, int b, int c){
    System.out.println(str);
    return a+b+c;
  }

  //addition
  public int calculate(int a, int b, String str, int c){
    System.out.println(str);
    return a+b+c;
  }

  public int calculate(int a, int b, int c, String str){
    System.out.println(str);
    return a+b+c;
  }

  public static void main(String[] args) {
    PolymorphismExp polymorphismExp = new PolymorphismExp();
    polymorphismExp.calculate(1,4);
  }

}
