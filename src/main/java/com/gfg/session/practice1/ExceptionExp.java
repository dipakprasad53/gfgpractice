package com.gfg.session.practice1;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

public class ExceptionExp {

  static String arr;

  public static void main(String[] args) throws NullPointerException {
    ExceptionExp exceptionExp = new ExceptionExp();
    try{
      System.out.println(arr.matches(""));
      int b = 4/0;
    }catch (NullPointerException e){
      System.out.println("Null pointer exception"+e);
    }
    catch (ArithmeticException e){
      e.printStackTrace();
      System.out.println("Arithmetic exception happened "+e);
    }catch (Exception e){
      System.out.println("Some other exception");
    }
//    finally {
//      System.out.println("Finally Block");
//    }

    int a = 10;
    if(a > 5){
      throw new RuntimeException("This is a new runtime exception");
    }


    try {
      check();
    }catch (Exception e){
      System.out.println(e);
    }finally {
      System.out.println("Finally");
    }
    System.out.println("End of program");
  }

  public static void check() throws Exception {
    try (InputStream ip = new FileInputStream("")){
      String s = null;
      File file = new File(s);
      System.out.println(file.delete());
    }catch (Exception e){
      System.out.println(e);
    }
    System.out.println("Checked");
  }

  // try with resource. -> Instances that have the capability of auto-close/closable.


}
