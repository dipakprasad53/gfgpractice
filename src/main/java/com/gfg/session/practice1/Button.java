package com.gfg.session.practice1;

public class Button {

  SmallButton smallButton ;

  private OnClickListener onClickListener;

  public void setOnClickListener(OnClickListener onClickListener){
    this.onClickListener = onClickListener;
  }
  public void getOnClickListener(){
    if(onClickListener != null){
      onClickListener.click();
    }
  }
  interface OnClickListener{
    public void click();
  }

  class SmallButton{
    public void disp(){
      System.out.println("print something");
    }
  }

  public static void main(String[] args) {
    Button button = new Button();
    button.setOnClickListener(new OnClickListener() {
      @Override
      public void click() {
        System.out.println("Button has been clicked");
      }
    });
    button.getOnClickListener();
  }
}
