package com.gfg.session.practice1;

public class GenericExp {
  public static void main(String[] args) {
    Box<Integer> aa = new Box<>(20);
    Box<String> bb = new Box<>("Ravish");
    Box<Double> cc = new Box<>(35.7);

    System.out.println(aa.getData());
    System.out.println(bb.getData());
    System.out.println(cc.getData());

  }
}

//When ever we come across similar usage of class but with different data types, we can go with generics.

class Box<D>{
  private D data;
  public Box(D d){
    data = d;
  }
  public void setData(D d){
    data = d;
  }
  public D getData(){
    return data;
  }
}
