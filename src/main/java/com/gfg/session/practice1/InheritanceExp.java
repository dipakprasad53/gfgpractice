package com.gfg.session.practice1;

public class InheritanceExp {


  public static void main(String[] args) {
    Child c1 = new Child(); //will be able to access all the properties from Father and child. Data reference will be from child class
    c1.displayInfo();
    c1.display();

    Father c2 = new Child(); //will be able to access all the properties from Father. Data reference will be from child class
    c2.fName = "Vishnu";
    c2.displayInfo();

    Father c3 = new Father(); //will be able to access all the properties from Father. Data reference will be from Father class
    c3.displayInfo();
//    Child c4 = new Father(); Incorrect reference

    Student.check();
  }
}
