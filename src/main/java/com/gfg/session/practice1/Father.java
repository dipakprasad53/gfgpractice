package com.gfg.session.practice1;

public class Father {
  String fName;
  String lName;

  String profession;
  public Father(){
    profession = "Mechanic";
  }

  //addition
  public int calculate(int a, int b, String str, int c){
    System.out.println(str);
    return a+b+c;
  }

  public void isPainter(){
    System.out.println("Is a great Artist");
  }

  public void displayInfo(){
    System.out.println("Name: "+fName+" "+lName);
    System.out.println("Profession: "+profession);
  }
}
