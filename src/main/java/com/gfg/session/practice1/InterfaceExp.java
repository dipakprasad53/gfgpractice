package com.gfg.session.practice1;

public class InterfaceExp implements Employee, Person{

  // Abstract class can not be instantiated -> We cannot create an object

  public static void main(String[] args) {
    InterfaceExp exp = new InterfaceExp();
    exp.display();
    exp.jobRole();
    System.out.println(Employee.getSalary(500.50, 30));
  }
  public void jobRole() {
    System.out.println("Engineer");
    Person.super.display();
  }

  public void display() {}


  public void play() {
    System.out.println("Cricket");
    display();
    Employee.super.display();
    Person.super.display();
  }
}

/*
 Interface properties
 1: Each method is abstract
 2: Multiple interface can be implemented
 3: 100% abstraction
 4: Public static & final variable's allowed
 5: Implementation of static and default methods available post Java 8.
 */

interface Employee{
  void jobRole();
  static double getSalary(double perDaySal, int numOfDays){
    return perDaySal*numOfDays;
  }
  default void display(){
    System.out.println("Nothing to display yet");
    System.out.println(getSalary(30,20));
  }
}

interface Person{
  public void play();
  public default void display(){
    System.out.println("Nothing to display again");
  }
}

interface Calculate{
  int cal(int a, int b, int c);
}

class FuncInterfaceExp implements Calculate{

  public static void main(String[] args) {
//    FuncInterfaceExp funcInterfaceExp = new FuncInterfaceExp();
//    System.out.println(funcInterfaceExp.cal(11,5, 5));

    Calculate add = (a,b,c) -> a*b;
    System.out.println(add.cal(12,5, 2));
  }

  @Override
  public int cal(int a, int b, int c) {
    return a*b;
  }
}

// inner class & anonymous inner class
class A{

  public static void main(String[] args) {
    A.B b = new A.B();
    b.display();

    C c = new C() {
      @Override
      public void display() {
        System.out.println("Anonymous inner class");
      }
      @Override
      public void display2() {

      }
    };

    Runnable r = new Runnable() {
      @Override
      public void run() {
        System.out.println("Inside runnable interface");
      }
    };
  }

  interface C{
    public void display();
    public void display2();
  }

  static class B{
    public void display(){
      System.out.println("my inner class");
    }
  }
}

