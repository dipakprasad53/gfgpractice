package com.gfg.session.jpa_hibernate.entities;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Type;

import java.sql.Date;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Getter
@Setter
@Entity(name="student_table_jpa")
public class Student {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(updatable = false, nullable = false, columnDefinition = "VARCHAR(36)", name = "student_id")
  @Type(type = "org.hibernate.type.UUIDCharType")
  private UUID studentId;

  @Column(nullable = false)
  private String name;

  private String contact="9876554321";

  private String branch;

  private String accountStatus = "ACTIVE";

  private Long registeredOn = System.currentTimeMillis(); //epoc timestamp

  private String password;

  private int studentAge;
}
