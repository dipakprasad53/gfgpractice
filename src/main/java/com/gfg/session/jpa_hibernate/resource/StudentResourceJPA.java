package com.gfg.session.jpa_hibernate.resource;
import com.gfg.session.jpa_hibernate.entities.Student;
import com.gfg.session.jpa_hibernate.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequestMapping("database/jpa")
public class StudentResourceJPA {

  @Autowired
  private StudentService studentService;

  @PostMapping("/add")
  public ResponseEntity<Object> addStudent(@RequestBody Student student){
    return studentService.addNewStudent(student);
  }

  @GetMapping("/get")
  public ResponseEntity<Object> getStudent(@RequestParam UUID uid){
    return studentService.getStudentById(uid);
  }

  @GetMapping("/get/2/{uid}")
  public ResponseEntity<Object> getStudentById(@PathVariable UUID uid){
    return studentService.getStudentById(uid);
  }



  /*
  git commit --all -m ""

  Homework
  Try to implement api's to
  1: get list of all students.
  2: get studentinfo using studentId.
  3: api to delete student.
  4: api to update student info.
   */
}

