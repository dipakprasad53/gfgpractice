package com.gfg.session.jpa_hibernate.repositories;

import com.gfg.session.jpa_hibernate.entities.Student;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;
import javax.transaction.Transactional;

@Repository
@Transactional
public interface StudentRepositories extends CrudRepository<Student, UUID> {
}
