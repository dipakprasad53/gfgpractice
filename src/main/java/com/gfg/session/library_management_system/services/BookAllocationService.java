package com.gfg.session.library_management_system.services;

import com.gfg.session.library_management_system.entities.AllocationRequest;
import com.gfg.session.library_management_system.entities.Book;
import com.gfg.session.library_management_system.entities.BookAllocation;
import com.gfg.session.library_management_system.entities.BookAllocationResponse;
import com.gfg.session.library_management_system.entities.Librarian;
import com.gfg.session.library_management_system.entities.Student;
import com.gfg.session.library_management_system.repositories.BookAllocationRepository;
import com.gfg.session.library_management_system.repositories.BookRepository;
import com.gfg.session.library_management_system.repositories.LibrarianRepository;
import com.gfg.session.library_management_system.repositories.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
public class BookAllocationService {

  @Autowired
  BookAllocationRepository bookAllocationRepository;

  @Autowired
  BookRepository bookRepository;

  @Autowired
  StudentRepository studentRepository;

  @Autowired
  LibrarianRepository librarianRepository;

  public BookAllocation allocateBook(AllocationRequest allocationRequest){
    Optional<Book> book = bookRepository.findById(allocationRequest.getBookInf());
    Optional<Student> student = studentRepository.findById(allocationRequest.getAllocatedTo());
    Optional<Librarian> librarian = librarianRepository.findById(allocationRequest.getIssuedBy());
    if(book.isPresent() && student.isPresent() && librarian.isPresent()){
      return bookAllocationRepository.save(BookAllocation.builder().allocatedTo(student.get())
          .issuedBy(librarian.get())
          .issuedOnTimeStamp(System.currentTimeMillis())
          .bookInf(book.get())
          .build());
    }else{
      return null;
    }
  }

  public BookAllocationResponse getBookAllocation(UUID id){
    BookAllocation allocation = bookAllocationRepository.findById(id).orElse(null);
    return BookAllocationResponse.builder()
        .bookAllocationId(allocation.getBookAllocationId())
        .allocatedTo(allocation.getAllocatedTo())
        .bookInf(allocation.getBookInf())
        .returnedStatus(allocation.isReturnedStatus())
        .returnTimeStamp(allocation.getReturnTimeStamp())
        .issuedBy(allocation.getIssuedBy())
        .build();
  }

}
