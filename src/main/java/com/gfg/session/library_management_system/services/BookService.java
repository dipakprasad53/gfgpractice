package com.gfg.session.library_management_system.services;

import com.gfg.session.library_management_system.entities.Book;
import com.gfg.session.library_management_system.repositories.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookService {

  @Autowired
  BookRepository bookRepository;

  public Book addBook(Book book){
    return bookRepository.save(book);
  }

  public Iterable<Book> addBookList(List<Book> book){
    return bookRepository.saveAll(book);
  }

//  public Book getBookById(UUID uuid){
//    return bookRepository.findById(uuid).orElse(null);
//  }
}
