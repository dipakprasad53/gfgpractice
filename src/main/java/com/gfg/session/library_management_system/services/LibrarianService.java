package com.gfg.session.library_management_system.services;

import com.gfg.session.library_management_system.entities.Librarian;
import com.gfg.session.library_management_system.repositories.LibrarianRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LibrarianService {

  @Autowired
  LibrarianRepository librarianRepository;

  public Librarian addLibrarian(Librarian librarian){
    return librarianRepository.save(librarian);
  }
}
