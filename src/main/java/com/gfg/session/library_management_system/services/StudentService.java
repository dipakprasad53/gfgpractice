package com.gfg.session.library_management_system.services;


import com.gfg.session.library_management_system.entities.Student;
import com.gfg.session.library_management_system.repositories.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class StudentService {


  @Autowired
  StudentRepository studentRepository;

  public Student addStudent(Student student){
    return studentRepository.save(student);
  }

  public Student getStudentById(UUID uuid){
    return studentRepository.findById(uuid).orElse(null);
  }



}
