package com.gfg.session.library_management_system.repositories;

import com.gfg.session.library_management_system.entities.BookAllocation;
import org.springframework.data.repository.CrudRepository;

import java.util.UUID;


public interface BookAllocationRepository extends CrudRepository<BookAllocation, UUID> {
}
