package com.gfg.session.library_management_system.repositories;

import com.gfg.session.library_management_system.entities.Student;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;
import javax.transaction.Transactional;

@Repository
@Transactional
public interface StudentRepository extends CrudRepository<Student, UUID> {
}
