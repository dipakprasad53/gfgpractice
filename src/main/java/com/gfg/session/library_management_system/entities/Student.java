package com.gfg.session.library_management_system.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Type;

import java.util.List;
import java.util.UUID;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity(name="student_table")
public class Student {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(updatable = false, nullable = false, columnDefinition = "VARCHAR(36)", name = "student_id")
  @Type(type = "org.hibernate.type.UUIDCharType")
  private UUID studentId;

  @Column(nullable = false)
  private String name;

  @Column(nullable = false)
  private String contact;

  private String branch;

  private String accountStatus = "ACTIVE";

  private Long registrationTimeStamp = System.currentTimeMillis(); //epoc timestamp

  @Column(nullable = false)
  private Long updateTimestamp = System.currentTimeMillis();
  private int age;

  @JsonIgnore
  @OneToMany(mappedBy = "allocatedTo", cascade = CascadeType.ALL)
  @JsonManagedReference
  private List<BookAllocation> allocatedBooks;
}
