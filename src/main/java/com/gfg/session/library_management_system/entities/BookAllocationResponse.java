package com.gfg.session.library_management_system.entities;

import lombok.Builder;
import lombok.Getter;

import java.util.UUID;

@Builder
@Getter
public class BookAllocationResponse {

  private UUID bookAllocationId;

  private Student allocatedTo;

  private Book bookInf;

  private Librarian issuedBy;

  private Long issuedOnTimeStamp;

  private boolean returnedStatus;

  private Long returnTimeStamp; //epoc timestamp

}
