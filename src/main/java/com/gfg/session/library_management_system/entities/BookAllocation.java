package com.gfg.session.library_management_system.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Type;

import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity(name="book_allocation_table")
public class BookAllocation {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(updatable = false, nullable = false, columnDefinition = "VARCHAR(36)", name = "book_allocation_id")
  @Type(type = "org.hibernate.type.UUIDCharType")
  private UUID bookAllocationId;

  @JoinColumn(updatable = false, nullable = false, columnDefinition = "VARCHAR(36)", name = "student_id")
  @JsonBackReference
  @ManyToOne(optional = false)
  @Type(type = "org.hibernate.type.UUIDCharType")
  private Student allocatedTo;

  @JoinColumn(updatable = false, nullable = false, columnDefinition = "VARCHAR(36)", name = "book_id")
  @ManyToOne(optional = false)
  @JsonBackReference
  @Type(type = "org.hibernate.type.UUIDCharType")
  private Book bookInf;

  @JoinColumn(updatable = false, nullable = false, columnDefinition = "VARCHAR(36)", name = "librarian_id")
  @JsonBackReference
  @ManyToOne(optional = false)
  @Type(type = "org.hibernate.type.UUIDCharType")
  private Librarian issuedBy;

  @Column(updatable = false, nullable = false, columnDefinition = "VARCHAR(36)", name = "issued_on")
  private Long issuedOnTimeStamp = System.currentTimeMillis(); //epoc timestamp

  @Column(nullable = false, name = "returned_status")
  private boolean returnedStatus = false;

  @Column(columnDefinition = "VARCHAR(36)", name = "return_on")
  private Long returnTimeStamp; //epoc timestamp

}
