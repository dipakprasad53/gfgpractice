package com.gfg.session.library_management_system.entities;

import lombok.Getter;
import lombok.Setter;

import java.util.UUID;


@Getter
@Setter
public class AllocationRequest {
  private UUID allocatedTo;
  private UUID bookInf;
  private UUID issuedBy;

}
