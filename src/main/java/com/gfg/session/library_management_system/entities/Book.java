package com.gfg.session.library_management_system.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Type;

import java.util.List;
import java.util.UUID;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity(name="books_table")
public class Book {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(updatable = false, nullable = false, columnDefinition = "VARCHAR(36)", name = "book_id")
  @Type(type = "org.hibernate.type.UUIDCharType")
  private UUID bookId;

  @Column(nullable = false)
  private String name;

  @Column(nullable = false)
  private String author;

  @Column(nullable = false)
  private boolean allocatedStatus = false;

  @JsonIgnore
  @OneToMany(mappedBy = "bookInf", cascade = CascadeType.ALL)
  @JsonManagedReference
  private List<BookAllocation> allocatedTo;


}
