package com.gfg.session.library_management_system.resources;

import com.gfg.session.library_management_system.entities.Book;
import com.gfg.session.library_management_system.entities.Librarian;
import com.gfg.session.library_management_system.entities.Student;
import com.gfg.session.library_management_system.repositories.LibrarianRepository;
import com.gfg.session.library_management_system.repositories.StudentRepository;
import com.gfg.session.library_management_system.services.BookService;
import com.gfg.session.library_management_system.services.LibrarianService;
import com.gfg.session.library_management_system.services.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("library/register")
public class RegistrationResource {

  @Autowired
  StudentService studentService;

  @Autowired
  LibrarianService librarianService;

  @Autowired
  BookService bookService;

  @PostMapping("/student")
    public ResponseEntity<Object> addStudent(@RequestBody Student student){
    return new ResponseEntity<>(studentService.addStudent(student), HttpStatus.CREATED);
  }

  @PostMapping("/{type}")
  public ResponseEntity<Object> addObject(@RequestBody Object obj, @PathVariable("type") String key) {
    switch (key) {
      case "student":
        return new ResponseEntity<>(studentService.addStudent((Student) obj), HttpStatus.CREATED);
      case "librarian":
        return new ResponseEntity<>(librarianService.addLibrarian((Librarian) obj), HttpStatus.CREATED);
      case "book":
        return new ResponseEntity<>(bookService.addBook((Book) obj), HttpStatus.CREATED);
      default:
        return new ResponseEntity<>("Invalid Request: Wrong key provided, Should be one " +
            "from [student,book,librarian]", HttpStatus.BAD_REQUEST);
    }
  }

  @PostMapping("/librarian")
  public ResponseEntity<Object> addLibrarian(@RequestBody Librarian librarian){
    return new ResponseEntity<>(librarianService.addLibrarian(librarian), HttpStatus.CREATED);
  }

  @PostMapping("/book")
  public ResponseEntity<Object> addBook(@RequestBody Book book){
    return new ResponseEntity<>(bookService.addBook(book), HttpStatus.CREATED);
  }

}
