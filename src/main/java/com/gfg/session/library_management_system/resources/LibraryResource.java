package com.gfg.session.library_management_system.resources;

import com.gfg.session.library_management_system.entities.AllocationRequest;
import com.gfg.session.library_management_system.entities.BookAllocation;
import com.gfg.session.library_management_system.entities.BookAllocationResponse;
import com.gfg.session.library_management_system.entities.Student;
import com.gfg.session.library_management_system.exceptions.InvalidRequestException;
import com.gfg.session.library_management_system.services.BookAllocationService;
import com.gfg.session.library_management_system.services.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequestMapping("library/")
public class LibraryResource {


  @Autowired
  BookAllocationService bookAllocationService;

  @Autowired
  StudentService studentService;

  @PostMapping("allocation")
  public ResponseEntity<Object> allocateBook(@RequestBody AllocationRequest allocationRequest){
    BookAllocation bookAllocation = bookAllocationService.allocateBook(allocationRequest);
    if(bookAllocation == null){
      throw new InvalidRequestException();
    }
    return new ResponseEntity<>(bookAllocation, HttpStatus.CREATED);
  }

  @GetMapping("allocation/get/{id}")
  public ResponseEntity<Object> getAllocation(@PathVariable("id") UUID id){
    BookAllocationResponse bookAllocation = bookAllocationService.getBookAllocation(id);
    if(bookAllocation == null){
      throw new InvalidRequestException();
    }
    return new ResponseEntity<>(bookAllocation, HttpStatus.FOUND);
  }

  @GetMapping("student/get/{id}")
  public ResponseEntity<Object> getStudentById(@PathVariable("id") UUID id){
    Student student = studentService.getStudentById(id);
    if(student == null){
      throw new InvalidRequestException();
    }
    return new ResponseEntity<>(student, HttpStatus.FOUND);
  }

}
