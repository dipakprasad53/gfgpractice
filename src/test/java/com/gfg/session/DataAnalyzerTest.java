package com.gfg.session;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.gfg.session.library_management_system.services.DataAnalyzer;
import com.gfg.session.library_management_system.services.DataService;
import org.junit.Test;

public class DataAnalyzerTest {
  @Test
  public void testFindMax() {
    DataService mockDataService = mock(DataService.class);
    when(mockDataService.retrieveData()).thenReturn(new int[]{5, 8, 2, 1, 10});

    DataAnalyzer dataAnalyzer = new DataAnalyzer(mockDataService);
    int result = dataAnalyzer.findMax();

    assertEquals(10, result);
    verify(mockDataService).retrieveData();
  }
}

