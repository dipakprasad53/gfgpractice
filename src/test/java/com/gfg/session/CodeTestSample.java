package com.gfg.session;

import com.gfg.session.library_management_system.entities.BookAllocation;
import com.gfg.session.library_management_system.entities.Student;
import com.gfg.session.library_management_system.repositories.BookAllocationRepository;
import com.gfg.session.library_management_system.services.BookAllocationService;
import com.gfg.session.practice3.DataAnalyzer;
import com.gfg.session.practice3.DataService;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

import java.util.Optional;
import java.util.UUID;

public class CodeTestSample {
  @Test
  public void testFindMax() {
    DataService mockDataService = mock(DataService.class);
    when(mockDataService.retrieveData()).thenReturn(new int[]{5, 8, 2, 1, 10});

    DataAnalyzer dataAnalyzer = new DataAnalyzer(mockDataService);
    int result = dataAnalyzer.findMax();

    assertEquals(10, result);
    verify(mockDataService).retrieveData();
  }

  @Test
  public void testBook(){
    BookAllocationRepository bookAllocationRepository = mock(BookAllocationRepository.class);
    UUID id =UUID.fromString("5bff7b74-1a53-4730-b61f-0f171dda047d");
    when(bookAllocationRepository.findById(id)).thenReturn(Optional.of(BookAllocation.builder()
            .allocatedTo(Student.builder().name("Dipak").age(29).build())
        .build()));

    BookAllocationService bookAllocationService = new BookAllocationService();
//    BookAllocationResponse bookAllocationResponse = bookAllocationService.getBookAllocation(id);
//    assertEquals("Dipak",bookAllocationResponse.getAllocatedTo().getName());
  }
}
